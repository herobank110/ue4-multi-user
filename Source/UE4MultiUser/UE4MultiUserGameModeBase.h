// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4MultiUserGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4MULTIUSER_API AUE4MultiUserGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
