# ue4 multi user

Test project for multiple users to edit an Unreal Engine 4 project at once.

# Launching the project

When launching the project, by default you will be prompted to rebuild it.
If you don't need to write any code, or don't have the compiler installed, you **should
not**
 do this.
 
Instead, first extract the *Binaries* folder from *Binaries.zip*, and then
launch the project.